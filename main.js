
const urlIp = "https://api.ipify.org/?format=json";
const infoUrl = "http://ip-api.com/json/";

const root = document.querySelector(".root");
const btnModal = document.querySelector(".modal-btn");

btnModal.addEventListener("click", async () => {
  const res = await fetch(urlIp);
  const { ip } = await res.json();
  const restwo = await fetch(`${infoUrl}${ip}`);
  const data = await restwo.json();

  const { timezone, country, regionName, city, region } = data;
  const elements = [timezone, country, regionName, city, region];

  elements.forEach((el) => {
    const paragraf = document.createElement("p");
    paragraf.innerText = el;
    root.append(paragraf);
  });
});

